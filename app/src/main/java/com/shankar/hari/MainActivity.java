package com.shankar.hari;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Creating button object and mapping it with the one that we created in XML
        Button myButton = (Button) findViewById(R.id.mainButton);
        //Setting up a listener for that button which will be triggered when button is clicked
        myButton.setOnClickListener(new View.OnClickListener() {

            //What should happen when button is clicked is defined here.
            @Override
            public void onClick(View v) {
                //Making and showing a toast.

                Toast.makeText(MainActivity.this, "Button Clicked!", Toast.LENGTH_LONG).show();
            }
        });

    }
}
